from datetime import datetime

from sqlalchemy import Column, DateTime

from configuration.database import db

__schema__ = "public"


class GenericDatabaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'schema': __schema__}

    created = Column(DateTime(), nullable=False, default=datetime.utcnow)
    modified = Column(DateTime(), nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
