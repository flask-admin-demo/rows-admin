from flask import flash
from flask_admin.actions import action
from flask_admin.babel import ngettext, gettext
from flask_admin.contrib.sqla import ModelView


class ContactView(ModelView):
    column_display_pk = True
    column_default_sort = [("modified", True)]
    # TODO: uncomment to show reorder
    # column_list = ("first_person_id", "second_person_id", "status", "created", "modified")

    @action('disassociate', 'Disassociate', 'Are you sure you want to disassociate the selected?')
    def action_disassociate(self, ids):
        try:
            count = 0
            for item in ids:
                first_person_id, second_person_id = item.split(',')
                contact = self.model.query.filter_by(first_person_id=first_person_id,
                                                     second_person_id=second_person_id).one()

                if contact.status != 0:
                    contact.status = 0
                    self.session.add(contact)
                    self.session.commit()
                    count += 1

            flash("%d contacts were disassociated" % count)
        except Exception as ex:
            if not self.handle_view_exception(ex):
                raise

            flash(gettext('Failed to disassociate contacts. %(error)s', error=str(ex)), 'error')
